import std/strformat

var numberOfOptions: int = 0
var options: seq[string]
var userInput: string
var option1: string
var option2: string

echo "enter the option you want to decide between"
echo "enter \"done\" when you're finished"
while true:
  echo fmt"number of options so far: {numberOfOptions}"
  echo "enter option: "
  userInput = readLine stdin

  if userInput == "done":
    if options.len < 2:
      echo "enter at least 2 options."
    else:
      # move on to option comparison
      break
  elif options.contains(userInput):
    echo "this option is already present. Nothing has been added."
  else:
    options.add(userInput)
    numberOfOptions += 1


echo "now let's compare the options in pairs to find your favorite"
echo "enter the number of your choice"
while len(options) > 1:
  option1 = options[0]
  option2 = options[1]
  echo "which do you like better:"
  echo fmt"1. {option1}"
  echo fmt"2. {option2}"
  user_input = stdin.readLine()
  if user_input == "1":
    options.del(1)
  elif user_input == "2":
    options.del(0)
  else:
    echo "enter \"1\" or \"2\""

echo fmt"your favorite option is: {options[0]}"
