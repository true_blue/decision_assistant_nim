# Package

version       = "0.1.0"
author        = "true blue"
description   = "a simple tool to help you make decisions."
license       = "AGPL-3.0-or-later"
srcDir        = "src"
bin           = @["decision_maker_nim"]


# Dependencies

requires "nim >= 1.7.1"
